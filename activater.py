#!/usr/bin/python3
import os
import subprocess
import datetime
import time
import notify2
count=1
start_time=datetime.time(5,0,0)
end_time=datetime.time(7,0,0)
go_sleep_time=datetime.time(9,0,0)
notify2.init("Activater.py")
noti=notify2.Notification("Activater.py","Started successfully")
noti.show()

try:
    while True:
        now=datetime.datetime.now()
        now_time=datetime.datetime.time(now)
        os.chdir(os.path.join(os.getenv("HOME"),'python/cli/morning'))
        if start_time<=now_time<=end_time and count:
           noti=notify2.Notification("Activater.py",'running morning.py')
           noti.show()
           subprocess.run('./morning.py',
               shell=True,
               check=True,
               )
           count=0
        
        if end_time<now_time and count==0:
            count=1
        time.sleep(3)

except KeyboardInterrupt:
    noti=notify2.Notification("Activater.py",'Quitting..')
    noti.show()
    exit()
except Exception as err:
    with open('logs.txt','a') as f:
        f.write(err)

