#!/usr/bin/python3
#Version:1.5
"""This script will do the following tasks every morning
0) log into the system
1) enable caffeine
2) turn wifi on 
3)open a terminal and execute the update script  and once done close it too....
4) Starts qbit-torrent
#More features will be added in future
"""

"""
To-Do:

"""
import os
import pyautogui
import time
import datetime
import notify2 #To show important notifications 
import subprocess
import re

from measureTime import measurer
wifiOn=False #Needed to decide whether or not to run update and qbittorrent

@measurer
def main(arg_list):
    func_list=[logIn,caffeine,wifi_set,updating,qbit]
    exec_list=[ func_list[pos] for pos,i in enumerate(arg_list) if not i]
    if logIn in exec_list:
        logIn()
        exec_list.remove(logIn)
    else:
        notification('Won\'t log in!')

    time.sleep(4)
    if pyautogui.locateCenterOnScreen('Desktop.png'):
        for i in exec_list:
                i()
        end() #show that all the activities have been completed and exit



@measurer
def logIn():
    time.sleep(6)
    root=pyautogui.locateCenterOnScreen('root.png')
    if not root:
        root=list(pyautogui.locateAllOnScreen('root.png'))
        try:
            root=root[0]
        except IndexError:
            while True:
                if pyautogui.locateCenterOnScreen('Desktop.png'):
                    notification("Already logged in!")
                    break
                else:
                    notification("Can't log in, do it manually")
                time.sleep(10)
    if root:
        os.chdir(os.path.join(os.path.pardir,'NoCommit'))
        with open('file.txt') as f:
            x=f.read()
            time.sleep(2)
            pyautogui.typewrite(x,interval=0.1)
        notification("Successfully logged In")
        os.chdir(os.path.join(os.path.pardir,'images')) #go back to images directory
    time.sleep(1)

#Turning Caffeine on
@measurer
def caffeine():
    #a full screen running app is needed to make sure that caffeine images are matched!
    pyautogui.hotkey('ctrl','alt','t')
    time.sleep(2)
    pyautogui.hotkey('winleft','up')
    time.sleep(2)
    caffeineCord=pyautogui.locateCenterOnScreen('Caffeine.png') or pyautogui.locateCenterOnScreen('Caffeine2.png')
    if caffeineCord:
        pyautogui.click(caffeineCord,duration=0.5)
        notification('Turned caffeine on')
    elif pyautogui.locateCenterOnScreen("CaffeineOn.png") or pyautogui.locateCenterOnScreen("CaffeineOn2.png") : 
        notification('Caffeine already enabled, so skipped.') 
    else:
        notification("Caffeine not found! \nIs it removed??")
    pyautogui.hotkey('alt','f4')
    return


#Turning wifi on
def settings():
    pyautogui.hotkey('ctrl','alt','s')
    time.sleep(3)


@measurer
def wifi_set():
    settings() 
    pyautogui.typewrite('wifi')
    pyautogui.press('enter')
    time.sleep(3)
    wifi_off=pyautogui.locateCenterOnScreen('wifi_off.png') or pyautogui.locateCenterOnScreen('wifi_off2.png')
    wifi_on=pyautogui.locateCenterOnScreen('connected.png')
    home_wifi=pyautogui.locateCenterOnScreen('home_wifi.png')

    global wifiOn
    if wifi_off:
        pyautogui.moveTo(wifi_off)
        pyautogui.moveRel((190,0))
        pyautogui.click()
        time.sleep(5)
        wifi_on=pyautogui.locateCenterOnScreen('connected.png')
        home_wifi=pyautogui.locateCenterOnScreen('home_wifi.png')
        if home_wifi :
            notification('Connected to home wifi') 
            wifiOn=True
        elif wifi_on:
            notification('Connected to unknown wifi!') 
            wifiOn=True
        else:
            notification("Could not connect to wifi")
            wifiOn=False
    elif wifi_on:
        if home_wifi:
            notification("Already connected to home wifi, skipped.")
        else:
            notification("Already Connected to unknown network")
        wifiOn=True
    #closing gnome-control-center
    close=pyautogui.locateCenterOnScreen('close.png') 
    pyautogui.click(close,duration=0.5)
    time.sleep(1)
                
@measurer
def updating():
    global wifiOn
    if wifiOn:
        notification('Updating the system')
        pyautogui.hotkey('ctrl','alt','t') #open a new terminal
        time.sleep(5)
        pyautogui.hotkey('enter') 
        scriptPath=os.path.join(os.path.abspath(os.path.pardir),'update')
        pyautogui.typewrite(f'{scriptPath}\n',interval=0.1)
        search=re.compile('.*(/bin/bash /root/python/cli/morning/update)+(.*)')
        while True:
            completed=subprocess.run(['ps','aux'],
                     stdout=subprocess.PIPE,)
            x=completed.stdout.decode('utf-8')
            res=re.search(search,x)
            if not res:
                break
        time.sleep(1.5)
        pyautogui.typewrite('exit\n',interval=0.1)
        notification("System updated successfully")
    else:
        notification("wifi not connected , updating system skipped")
    time.sleep(1)

def qbit():
    global wifiOn
    if wifiOn:
        notification("started qBittorrent")
        pyautogui.hotkey('ctrl','alt','q')
    else:
        notification("wifi not connected, qbit not started")
    time.sleep(1)


def notification(string):
    notify=notify2.Notification("Morning.py",string)
    notify.show()

def end():
    global wifiOn
    wifiOn=False
    notification('Run successfully')
    exit()

#Initializing
if __name__ =='__main__':
    import argparse 
    parser=argparse.ArgumentParser()
    parser.add_argument("-c","--caffeine",help="Disables caffeine enabling feature ",action="store_true")
    parser.add_argument("-l","--login",help="Disables logging in ",action="store_true")
    parser.add_argument("-u","--update",help="Disables updating system ",action="store_true")
    parser.add_argument("-w","--wifi",help="Disables turning wifi-on ",action="store_true")
    parser.add_argument("-q","--qBittorrent",help="Disables starting qBittorrent",action="store_true")
    args=parser.parse_args()
    arg_list=[args.login,args.caffeine,args.wifi,args.update,args.qBittorrent]
    os.chdir('/root/python/cli/morning')
    os.chdir("images") #images are in this directory
    notify2.init("Morning.py") #Initializing is important to show  notifications
    notification("Started successfully")
    try:
            main(arg_list)
    except KeyboardInterrupt : #catching CTRL+C
        notification("Quitted forcefully")
        exit()
    except Exception as ex:
        notification("Error ocurred! Quitting...")
        exit()
        
