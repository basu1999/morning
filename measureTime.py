import time
import notify2
def measurer(func):
    """A decorator function which will give notification
    about how long it took to run a function"""
    took=0
    def inner(*args,**kwargs):
        start=time.time()
        call=func(*args,**kwargs)
        took=time.time()-start
        notification=notify2.Notification(f"{func.__name__} took {took:.2f} seconds") 
        notification.show()
        return call
    return inner

